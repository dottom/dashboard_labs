# Dashboard CMS
![Design](https://mir-s3-cdn-cf.behance.net/project_modules/1400_opt_1/001f14101319839.5f1c0f2191e73.png)
Gambar design bisa dilihat di [Behance](https://www.behance.net/gallery/101319839/Dashboard-CMS-Design-Simple-and-Fresh)

# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version

* System dependencies

* Configuration

* Database creation

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* ...

## Site Map Information
  * ### Branch Master
    -  Framework RoR
    -  Framework CSS Materialize
  
  * ### Branch Design
    - Slicingan Framework CSS Materialize
    
## Harapannya
Saya bisa memfungsingkan dashboard minimal CRUD

> Semoga Project ini bisa selesai tahun ini. ~amin :metal:
